# laravel8-php8-config

Configuración de laravel versión 8 como API Rest con php 8 y Mysql 8

- Copiar variables de entorno y personalizar cp .env.example .env
- Colocar el mismo nombre de servidor de .env=>VIRTUAL_HOST en el server_name de ./siteConfig/nginx/conf.d/app.conf
- Clonar el repositorio del proyecto de laravel dentro del directorio ./laravel-app
- Construir, crear e iniciar los contenedores docker-compose up -d

